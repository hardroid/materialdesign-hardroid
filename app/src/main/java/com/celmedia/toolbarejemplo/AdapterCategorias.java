package com.celmedia.toolbarejemplo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by harce on 18-02-2015.
 */
public class AdapterCategorias extends RecyclerView.Adapter<CategoriasRowHolder> {

    private List<Categorias> values;
    private Context mContext;


    public AdapterCategorias(Context context, List<Categorias> values) {
        this.values = values;
        this.mContext = context;
    }

    @Override
    public CategoriasRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, null);
        CategoriasRowHolder item = new CategoriasRowHolder(v);
        return item;
    }

    @Override
    public void onBindViewHolder(CategoriasRowHolder holder, int position) {
        Categorias catItem = values.get(position);
        holder.setName(catItem.getCategoria());
    }

    @Override
    public int getItemCount() {
        return (values != null ? values.size() : 0);
    }


}
