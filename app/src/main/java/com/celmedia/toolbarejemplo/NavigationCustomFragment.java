package com.celmedia.toolbarejemplo;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationCustomFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterCategorias adapter;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarToggle;

    private static final String FILE_PREF_NAME = "pref_app";
    private static final String KEY_DRAWER = "key_drawer";

    private boolean mUserLearnerDrawer;
    private boolean mFromSavedInstanceState;
    private View containerView;

    public NavigationCustomFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstaceState){
        super.onCreate(savedInstaceState);
        mUserLearnerDrawer = readToPreferences(getActivity(), KEY_DRAWER, false);
        if(savedInstaceState != null){
            mFromSavedInstanceState = true;
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_navigation_custom, container, false);
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerViewList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdapterCategorias(getActivity(),getCategoriasDefault());
        recyclerView.setAdapter(adapter);
        return v;
    }

    private List<Categorias> getCategoriasDefault(){
        List<Categorias> values = new ArrayList<Categorias>();
        values.add(new Categorias("uno"));
        values.add(new Categorias("dos"));
        values.add(new Categorias("tres"));
        values.add(new Categorias("cuatro"));
        return values;
    }


    public void setUp(int fragmentID, DrawerLayout drawer, final Toolbar toolbar) {

        containerView = getActivity().findViewById(fragmentID);

        mDrawerLayout = drawer;

        mActionBarToggle = new ActionBarDrawerToggle(getActivity(),
                                                     drawer,
                                                     R.string.drawer_open,
                                                     R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if(!mUserLearnerDrawer){
                    mUserLearnerDrawer = true;
                    saveToPreferences(getActivity(),KEY_DRAWER,mUserLearnerDrawer);
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if(slideOffset < 0.6){
                    toolbar.setAlpha((1-slideOffset));
                }
            }
        };
        mDrawerLayout.setDrawerListener(mActionBarToggle);

        if(!mUserLearnerDrawer && !mFromSavedInstanceState){
            mDrawerLayout.openDrawer(containerView);
        }

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarToggle.syncState();
            }
        });



    }

    public static void saveToPreferences(Context context, String preferenceName, boolean preferenceValue) {
        SharedPreferences pref = context.getSharedPreferences(FILE_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean(preferenceName, preferenceValue);
        edit.apply();
    }

    public static boolean readToPreferences(Context context, String preferenceName, boolean defaultValue) {
        SharedPreferences pref = context.getSharedPreferences(FILE_PREF_NAME, Context.MODE_PRIVATE);
       return pref.getBoolean(preferenceName, defaultValue);
    }

}
