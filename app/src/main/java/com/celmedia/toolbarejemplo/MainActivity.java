package com.celmedia.toolbarejemplo;

import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    Toolbar toolbar;
    NavigationCustomFragment navigation;
    DrawerLayout drawer;
    View containerDrawer;

    ViewPager pager;
    SlidingTabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar)findViewById(R.id.toolbar_custom);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigation = (NavigationCustomFragment)getSupportFragmentManager().findFragmentById(R.id.fragmenNavigation);
        containerDrawer = findViewById(R.id.fragmenNavigation);
        drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        navigation.setUp(R.id.fragmenNavigation, drawer, toolbar);

        tabs = (SlidingTabLayout)findViewById(R.id.tabs);
        pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new MyViewPagerAdapter(getSupportFragmentManager()));
        tabs.setViewPager(pager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            drawer.openDrawer(containerDrawer);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class MyViewPagerAdapter extends FragmentPagerAdapter{
        String [] tabsName;
        MyViewPagerAdapter(FragmentManager fm) {
            super(fm);
            tabsName = getResources().getStringArray(R.array.tabs_name);
        }

        @Override
        public Fragment getItem(int position) {
            MyFragmentNoob myFragmentNoob = MyFragmentNoob.getInstance(position);
            return myFragmentNoob;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabsName[position];
        }

        @Override
        public int getCount() {
            return tabsName.length;
        }
    }

    public static class MyFragmentNoob extends Fragment{

        private TextView txtDescription;

        public static MyFragmentNoob getInstance(int position) {
            MyFragmentNoob frag = new MyFragmentNoob();
            Bundle args = new Bundle();
            args.putInt("position", position);
            frag.setArguments(args);
            return  frag;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v = inflater.inflate(R.layout.layout_noob, container, false);

            txtDescription = (TextView)v.findViewById(R.id.textViewDescripcion);

            Bundle params = getArguments();
            if(params != null){
                txtDescription.setText("Fragment n° " + params.getInt("position"));
            }

            return v;
        }
    }
}
