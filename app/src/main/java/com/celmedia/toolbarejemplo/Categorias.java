package com.celmedia.toolbarejemplo;

/**
 * Created by harce on 18-02-2015.
 */
public class Categorias {

    private String categoria;

    public Categorias(){
        categoria = "default";
    }

    public Categorias(String categoria){
        this.categoria = categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
