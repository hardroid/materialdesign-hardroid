package com.celmedia.toolbarejemplo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by harce on 18-02-2015.
 */
public class CategoriasRowHolder extends RecyclerView.ViewHolder {

    private ImageView icon;
    private TextView name;

    public CategoriasRowHolder(View itemView) {
        super(itemView);

        this.icon = (ImageView)itemView.findViewById(R.id.icon_menu);
        this.name = (TextView)itemView.findViewById(R.id.txtNameMenu);
    }

    public void setName(String name){
        this.name.setText(name);
    }
}
